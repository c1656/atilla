import photo1 from "../../assets/images/carousel/black-home.jpg";
import photo2 from "../../assets/images/carousel/home-dark.jpg";
import photo3 from "../../assets/images/carousel/white-home.jpg";
import "./style.scss";

const Carousel = () => {
  const items = [
    { image: photo1, alt: "..." },
    { image: photo2, alt: "..." },
    { image: photo3, alt: "..." },
  ];
  return (
    <section id="carousel">
      <div className="container-fluid p-0">
        <div
          id="carouselExampleAutoplaying"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-inner">
            {items.map(({ image, alt }, index) => (
              <div class="carousel-item active" key={index}>
                <img src={image} class="d-block w-100" alt={alt} />
              </div>
            ))}
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleAutoplaying"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleAutoplaying"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </section>
  );
};

export default Carousel;
