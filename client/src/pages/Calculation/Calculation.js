import AreaRange from "../../components/AreaRange/AreaRange";
import { useTranslation } from "react-i18next";
import "./style.scss";

export default function Calculation() {
  const { t } = useTranslation("translation", { keyPrefix: "calculation" });
  return (
    <section className="calculation">
      <div className="container">
        <h2>{t("title")}</h2>
        <div className="row">
          <div className="col-12 col-md-6">
            <p>{t("text1")}</p>
            <p>{t("text2")}</p>
          </div>
          <div className="col-12 col-md-6 center">
            <AreaRange />
          </div>
        </div>
      </div>
    </section>
  );
}
