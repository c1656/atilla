import React from "react";
import { useTranslation } from "react-i18next";
import "./style.scss";

const About = () => {
  const { t } = useTranslation("translation", { keyPrefix: "about" });

  return (
    <section id="about" className="about">
      <div className="container">
        <h2>{t("title")}</h2>
        <div className="row">
          <div className="col-12 col-md-6">
            <p>{t("text1")}</p>
            <p>{t("text2")}</p>
          </div>
          <div className="col-12 col-md-6">
            <p>{t("text3")}</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;
