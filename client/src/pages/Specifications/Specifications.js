import React from "react";
import heater from "../../assets/icons/specifications/heat-pump.png";
import area from "../../assets/icons/specifications/area.png";
import energy from "../../assets/icons/specifications/energy.png";
import safety from "../../assets/icons/specifications/safety.png";
import framework from "../../assets/icons/specifications/framework.png";
import multistorey from "../../assets/icons/specifications/multistorey.png";
import height from "../../assets/icons/specifications/height.png";
import soundproof from "../../assets/icons/specifications/soundproof.png";
import { useTranslation } from "react-i18next";
import "./style.scss";

export default function Specifications() {
  const { t } = useTranslation();

  const specifications = [
    { icon: heater, text: t("specifications.heating") },
    { icon: area, text: t("specifications.area") },
    { icon: energy, text: t("specifications.utilities") },
    { icon: safety, text: t("specifications.alarm") },
    { icon: framework, text: t("specifications.framework") },
    { icon: multistorey, text: t("specifications.floors") },
    { icon: height, text: t("specifications.height") },
    { icon: soundproof, text: t("specifications.soundproof") },
  ];

  const renderTextWithLineBreaks = (text) => {
    // Split the text by \n and map over it to return an array of lines with <br /> in between
    return text.split("\n").map((line, index, array) => (
      <React.Fragment key={index}>
        {line}
        {index < array.length - 1 ? <br /> : null}
      </React.Fragment>
    ));
  };

  return (
    <section className="specifications">
      <div className="container">
        <h2>{t("specifications.title")}</h2>
        <div className="row">
          {specifications.map(({ icon, text }, index) => (
            <div className="col col-md-6 col-lg-3" key={index}>
              <div className="specification">
                <div className="wrapper">
                  <img className="icon" src={icon} alt={t(text)} />
                </div>
                <span>{renderTextWithLineBreaks(t(text))}</span>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
