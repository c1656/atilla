import image1 from "../../assets/images/image1.jpg";
import image2 from "../../assets/images/image2.jpg";
import image3 from "../../assets/images/image3.jpg";
import image4 from "../../assets/images/image4.jpg";
import { useTranslation } from "react-i18next";
import "./style.scss";

export default function Advantages() {
  const { t } = useTranslation("translation", { keyPrefix: "advantages" });

  const items = [
    { text: t("text1"), textTitle: t("text1title"), image: image1 },
    { text: t("text2"), textTitle: t("text2title"), image: image2 },
    { text: t("text3"), textTitle: t("text3title"), image: image3 },
    { text: t("text4"), textTitle: t("text4title"), image: image4 },
  ];

  return (
    <section className="advantages">
      <div className="container">
        <h2>{t("title")}</h2>
        <div className="row">
          {items.map(({ text, textTitle, image }) => (
            <div className="col-12 col-md-6 wrapper" key={textTitle}>
              <img src={image} alt={textTitle} />
              <p>
                <span>{textTitle} </span>
                {text}
              </p>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
