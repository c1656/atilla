import { useState } from "react";
import "./style.scss";
import { sendFormToTelegram } from "../../utils/sendFormToTelelgram";
import { useTranslation } from "react-i18next";
import formIsSentIcon from "../../assets/icons/circle-check-solid.svg";

export default function Form() {
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    name: "",
    phone: "",
    message: "",
  });
  const { t } = useTranslation("translation", { keyPrefix: "form" });
  const [formIsSent, setFormIsSent] = useState(false);

  const validateForm = () => {
    const newErrors = {};
    if (!formData.name) newErrors.name = t("nameError");
    if (
      !formData.phone ||
      !/^\+?\d{1,3}?[ -]?\d{1,3}[ -]?\d{4,10}$/.test(formData.phone)
    )
      newErrors.phone = t("phoneError");

    if (!formData.message) newErrors.message = t("messageError");
    setErrors(newErrors);
    return Object.keys(newErrors).length === 0;
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
    if (errors[name]) {
      setErrors({ ...errors, [name]: "" });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (validateForm()) {
      sendFormToTelegram(
        `Name: ${formData.name}, Phone: ${formData.phone}, Message: ${formData.message}`
      );
      setFormIsSent(true);
    }
  };

  return (
    <section className="form">
      <div className="container">
        <div className="wrapper">
          {formIsSent ? (
            <div className="form-is-sent">
              <img src={formIsSentIcon} alt="circle-check-solid" />
              <h3>{t("submitted")}</h3>
            </div>
          ) : (
            <>
              {" "}
              <h3>{t("title")}</h3>
              <form onSubmit={handleSubmit}>
                <label htmlFor="name">{t("name")}</label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  value={formData.name}
                  onChange={handleChange}
                  className={
                    errors.name ? "form-control" : "form-control input-margin"
                  }
                />
                {errors.name && <span className="error">{errors.name}</span>}

                <label htmlFor="phone">{t("phone")}</label>
                <input
                  type="text"
                  id="phone"
                  name="phone"
                  value={formData.phone}
                  onChange={handleChange}
                  className={
                    errors.phone ? "form-control" : "form-control input-margin"
                  }
                />
                {errors.phone && <span className="error">{errors.phone}</span>}
                <label htmlFor="message">{t("message")}</label>
                <textarea
                  id="message"
                  name="message"
                  value={formData.message}
                  onChange={handleChange}
                  className={
                    errors.message
                      ? "form-control"
                      : "form-control input-margin"
                  }
                />
                {errors.message && (
                  <span className="error">{errors.message}</span>
                )}

                <button className="submit-button" type="submit">
                  {t("submit")}
                </button>
              </form>
            </>
          )}
        </div>
      </div>
    </section>
  );
}
