import React from "react";
import "./style.scss";
import { useTranslation } from "react-i18next";

const Home = () => {
  const { t } = useTranslation("translation", { keyPrefix: "home" });
  return (
    <section className="home">
      <div className="container">
        <h4>{t("slogan")}</h4>
        <h1>{t("title")}</h1>
      </div>
    </section>
  );
};

export default Home;
