import React, { useState, useEffect } from "react";
import LanguageSwitcher from "../LanguageSwitcher/LanguageSwitcher";
import { useTranslation } from "react-i18next";
import "./style.scss";

const Header = () => {
  const [scrolled, setScrolled] = useState(false);
  const { t } = useTranslation("translation", { keyPrefix: "header" });

  const handleScroll = () => {
    const offset = window.scrollY;
    if (offset > 50) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <section className={`header ${scrolled ? "scrolled" : ""}`}>
      <nav className="navbar navbar-expand-lg">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link" aria-current="page" href="#about">
                  {t("about")}
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#planning">
                  {t("planning")}
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#news">
                  {t("news")}
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#form">
                  {t("contacts")}
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link phone" href="tel:+380951393968">
                  +380951393968
                </a>
              </li>
              <li className="nav-item">
                <LanguageSwitcher />
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </section>
  );
};

export default Header;
