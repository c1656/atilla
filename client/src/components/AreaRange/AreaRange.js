import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./style.scss";

export default function AreaRange() {
  const [area, setArea] = useState(35);
  const [price, setPrice] = useState(calculatePrice(35));
  const { t } = useTranslation("translation", { keyPrefix: "calculation" });

  function calculatePrice(area) {
    return area * 1000;
  }

  useEffect(() => {
    setPrice(calculatePrice(area));
  }, [area]);

  const handleAreaChange = (e) => setArea(e.target.value);

  return (
    <div className="area-range">
      <label>
        {t("totalArea")} {area} m²
        <input
          type="range"
          value={area}
          min="35"
          max="180"
          onChange={handleAreaChange}
        />
        <div className="values">
          <span>35 m²</span>
          <span>180 m²</span>
        </div>
      </label>
      <div className="price">
        <span>{price}</span>
      </div>
    </div>
  );
}
