import React from "react";
import { useTranslation } from "react-i18next";
import "./style.scss";

function LanguageSwitcher() {
  const { i18n } = useTranslation();

  const changeLanguage = () => {
    i18n.language === "en"
      ? i18n.changeLanguage("pl")
      : i18n.changeLanguage("en");
  };

  return (
    <button className="language-swithcer" onClick={() => changeLanguage()}>
      {i18n.language === "en" ? "pl" : "en"}
    </button>
  );
}

export default LanguageSwitcher;
