import "./styles/main.scss";
import Header from "./components/Header/Header";
import Home from "./pages/Home/Home";
import About from "./pages/About/About";
import Form from "./pages/Form/Form";
import Advantages from "./pages/Advantages/Advantages";
import Specifications from "./pages/Specifications/Specifications";
import Calculation from "./pages/Calculation/Calculation";
import Carousel from "./pages/Carousel/Carousel";

function App() {
  return (
    <div className="App">
      <Header />
      <Home />
      <About />
      <Form />
      <Advantages />
      <Specifications />
      <Calculation />
      <Carousel />
    </div>
  );
}

export default App;
