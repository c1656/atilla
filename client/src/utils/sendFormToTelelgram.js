export async function sendFormToTelegram(messageText) {
  const url = "http://localhost:5001/api/submit-form";
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ text: messageText }),
    });
  } catch (error) {
    console.error("Error sending message:", error);
  }
}
