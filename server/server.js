const express = require("express");
const axios = require("axios");
const cors = require("cors");
require("dotenv").config();
const app = express();
const port = process.env.PORT || 5001;

app.use(
  cors({
    origin: "http://localhost:3000",
  })
);
app.use(express.json());

app.get("/api/hello", (req, res) => {
  res.json({ message: "Hello from Express.js!" });
});

app.post("/api/submit-form", async (req, res) => {
  const text = req.body.text;
  const chatId = process.env.CHAT_ID; // Ensure these are set in your server environment
  const token = process.env.TELEGRAM_BOT_TOKEN;
  const url = `https://api.telegram.org/bot${token}/sendMessage`;

  try {
    const response = await axios.post(url, {
      chat_id: chatId,
      text: text,
    });
    res.send(response.data);
  } catch (error) {
    console.error(
      "Error sending message to Telegram:",
      error.response ? error.response.data : error
    );
    res.status(500).send("Failed to send message to Telegram");
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
